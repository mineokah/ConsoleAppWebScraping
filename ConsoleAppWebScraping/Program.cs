﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Net;
using System.Text.RegularExpressions;
using System.Collections.Specialized;

namespace ConsoleAppWebScraping
{
    class Program
    {
        static void Main(string[] args)
        {
            //// トップページ
            //var url = "https://www.green-japan.com/";

            //// ログインページ
            //url = "https://www.green-japan.com/login";
            //url = "https://www.green-japan.com/create";

            /*
            メールアドレス入力欄
            <input type="text" name="user[mail]" id="user_mail">
            パスワード入力欄
            <input type="password" name="user[password]" id="user_password">
            ログインボタン
            <input type="submit" name="commit" value="ログイン" data-disable-with="ログイン中...">
            */

            //var wb = new WebClient();
            //var data = new NameValueCollection();
            //var loginUrl = "https://www.green-japan.com/create";
            //data["user_mail"] = "tekitou@tekitou.com";
            //data["user_password"] = "tekitou";

            //var response = wb.UploadValues(loginUrl, "POST", data);

            //System.Net.ServicePointManager.Expect100Continue = false;

            //HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);

            //// UserAgentの指定 ←◆これがないと403 forbidden が返ってくる
            //req.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36";

            //// requestにpostを指定する
            //req.Method = "POST";
            //req.ContentType = "application/x-www-form-urlencoded";
            //req.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9";
            ////req.Accept = "*/*";

            //// Refererの指定 ←◆これがないと 422 Unprocessable Entity が返ってくる
            //req.Referer = "https://www.green-japan.com/";

            //// POSTするデータ
            //// ChromeのConsole→NetWork→Doc→create→Header→From Dataから参照した。
            ////var postData = "user_mail=" + Uri.EscapeDataString("tekitou");
            ////postData += "&user_password=" + Uri.EscapeDataString("tekitou");
            ////var postData = "utf8" + "=" + Uri.EscapeDataString("✓");
            ////postData += @"authenticity_token =" + Uri.EscapeDataString("rr17syuWAxOibUnw3OgFQR+StmYXyJr0X+JWsY/W9ZsNGtLh0FLUohA3WV5g5O/WwiKPeA+tbY2AwkYrUsEmTA==");
            ////postData += @"&authenticity_token=" + Uri.EscapeDataString("rr17syuWAxOibUnw3OgFQR+StmYXyJr0X+JWsY/W9ZsNGtLh0FLUohA3WV5g5O/WwiKPeA+tbY2AwkYrUsEmTA==");
            //var postData = Uri.EscapeDataString("user[mail]") + "=" + Uri.EscapeDataString("mailAddress");
            //postData += "&" + Uri.EscapeDataString("user[password]") + "=" + Uri.EscapeDataString("password");
            ////postData += "&" + Uri.EscapeDataString("commit") + "=" + Uri.EscapeDataString("ログイン");
            ////postData += "&" + Uri.EscapeDataString("target_url") + "=" + Uri.EscapeDataString("");


            //// byte変換した送信データ
            //var data = Encoding.ASCII.GetBytes(postData);
            //data = Encoding.UTF8.GetBytes(postData);
            //CookieContainer cc = new CookieContainer();
            //req.CookieContainer = cc;

            //// 送信データの長さ指定
            //req.ContentLength = data.Length;

            //var html = string.Empty;

            //// POST実行
            ////WebResponse res = req.GetResponse();
            ////Stream resStream = res.GetResponseStream();
            //Stream reqStream = req.GetRequestStream();
            //reqStream.Write(data, 0, data.Length);
            //reqStream.Close();

            // ↑20211101_2337時点において、422エラーが返る。
            // サーバ側が、想定する値でない場合に返すエラーらしい。
            // requestのheader値が異なる、ということであるとひとまず理解する。これが正しい見解であるかは、不明。

            // 転職サイトgreenにログインする場合の必須項目

            var url = "https://www.green-japan.com/create";
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);

            // UserAgentの指定 ←◆これがないと403 forbidden が返ってくる
            req.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36";

            // requestにpostを指定する
            req.Method = "POST";
            req.ContentType = "application/x-www-form-urlencoded";
            req.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9";
            //req.Accept = "*/*";

            // Refererの指定 ←◆これがないと 422 Unprocessable Entity が返ってくる
            req.Referer = "https://www.green-japan.com/";

            // cookie ログイン後たぶん必要になる
            CookieContainer cc = new CookieContainer();
            req.CookieContainer = cc;

            // POSTするデータ
            var postData = Uri.EscapeDataString("user[mail]") + "=" + Uri.EscapeDataString("mail@address.com");
            postData += "&" + Uri.EscapeDataString("user[password]") + "=" + Uri.EscapeDataString("password");

            // byte変換した送信データ
            var data = Encoding.ASCII.GetBytes(postData);
            data = Encoding.UTF8.GetBytes(postData);

            // 送信データの長さ指定
            req.ContentLength = data.Length;

            // POST実行
            Stream reqStream = req.GetRequestStream();
            reqStream.Write(data, 0, data.Length);
            reqStream.Close();

            // レスポンス取得
            WebResponse res = req.GetResponse();
            Stream resStream = res.GetResponseStream();

            Encoding encoder = Encoding.GetEncoding("UTF-8");
            StreamReader sr = new StreamReader(resStream, encoder);
            var result = sr.ReadToEnd();
            sr.Close();
            resStream.Close();
            Console.WriteLine(result);

            // ↑でログインが完了し、
            // ↓でログイン後に閲覧可能なページを取得する。
            req.Method = "GET";
            //url = @"https://www.green-japan.com/mypage01?case=login";
            url = @"https://www.green-japan.com/favorites/simple?sort=only_client_for_user";
            var req2 = (HttpWebRequest)WebRequest.Create(url);
            req2.Referer = req.Referer;
            req2.CookieContainer = req.CookieContainer;
            req2.Accept = req.Accept;
            req2.UserAgent = req.UserAgent;

            // GET実行
            HttpWebResponse httpWebResponse = (HttpWebResponse)req2.GetResponse();
            Stream receiveStream = httpWebResponse.GetResponseStream();
            StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);
            httpWebResponse.Close();
            result = readStream.ReadToEnd();
            readStream.Close();
            Console.WriteLine(result);

            Console.WriteLine("hoge");

            //reqStream = req2.GetRequestStream();
            //reqStream.Write(data, 0, data.Length);
            //reqStream.Write()
            //reqStream.Close();

            //// レスポンス取得
            //WebResponse res = req.GetResponse();
            //Stream resStream = res.GetResponseStream();

            //Encoding encoder = Encoding.GetEncoding("UTF-8");
            //StreamReader sr = new StreamReader(resStream, encoder);
            //var result = sr.ReadToEnd();
            //sr.Close();
            //resStream.Close();
            //Console.WriteLine(result);


            //using (var res = (HttpWebResponse)req.GetResponse())
            //using (var st = res.GetResponseStream())
            //using (var sr = new StreamReader(st, Encoding.UTF8))
            //using (var stream = req.GetRequestStream())
            //{
            //    //html = sr.ReadToEnd();
            //    stream.Write(data, 0, data.Length);


            //}

            //var response = (HttpWebResponse)req.GetResponse();



            //Console.WriteLine(response.ToString());



        }
    }
}
